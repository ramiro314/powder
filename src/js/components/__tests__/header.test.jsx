import React from 'react';
import { StaticRouter } from 'react-router';
import { mount } from 'enzyme';

import { Header } from 'components';
import { LocalStorageMock } from 'mocks';
import { jwt } from 'mocks/constants';

const getRootComponent = (pathname = '/login') => mount((
  <StaticRouter context={{}} location={pathname}>
    <Header />
  </StaticRouter>
));

describe('header', () => {
  it('renders properly on /login', () => {
    const root = getRootComponent();
    expect(root.find('IconButton')).toHaveLength(1);
    expect(root.find('Link')).toHaveLength(0);
  });

  it('renders properly on /reset-password', () => {
    const root = getRootComponent('/reset-password');
    expect(root.find('IconButton')).toHaveLength(1);
    expect(root.find('Link')).toHaveLength(1);
    expect(root.find('Link').prop('to')).toBe('/login');
  });

  it('renders properly on /account', () => {
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt);
    const root = getRootComponent('/reset-password');
    expect(root.find('IconButton')).toHaveLength(1);
    expect(root.find('Link')).toHaveLength(1);
    expect(root.find('Link').prop('to')).toBe('/account');
    localStorageMock.deactivate();
  });

  it('animates on click', () => {
    const root = getRootComponent();
    root.find('IconButton').simulate('click');
    expect(root.update().find('IconButton img').hasClass('gnarshake')).toBe(true);
    return new Promise(resolve => {
      setTimeout(() => {
        expect(root.update().find('IconButton img').hasClass('gnarshake')).toBe(false);
        resolve();
      }, 1000);
    });
  });

  it('ignores additional clicks during the animation cycle', () => {
    const root = getRootComponent();
    root.find('IconButton').simulate('click');
    expect(root.update().find('IconButton img').hasClass('gnarshake')).toBe(true);
    return new Promise(resolve => {
      setTimeout(() => {
        expect(root.update().find('IconButton img').hasClass('gnarshake')).toBe(true);
        root.find('IconButton').simulate('click');
        setTimeout(() => {
          expect(root.update().find('IconButton img').hasClass('gnarshake')).toBe(false);
          resolve();
        }, 500);
      }, 500);
    });
  });
});
