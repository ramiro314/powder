import { isLoggedIn } from 'gnar-edge/es/jwt';
import IconButton from '@material-ui/core/IconButton';
import Link from 'react-router-dom/Link';
import React, { PureComponent } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import classnames from 'classnames';
import withRouter from 'react-router-dom/withRouter';

import logo from 'assets/images/logo.png';

import './index.scss';

@withRouter
export default class Header extends PureComponent {
  static propTypes = {
    location: ReactRouterPropTypes.location.isRequired
  }

  state = {
    gnarshake: false
  }

  animate = () => {
    if (!this.state.gnarshake) {
      this.setState({ gnarshake: true });
      setTimeout(() => this.setState({ gnarshake: false }), 1000);
    }
  }

  render() {
    const { gnarshake } = this.state;
    const logoImg = (
      <IconButton className='header' onClick={this.animate}>
        <img alt='Gnar Logo' className={classnames({ gnarshake })} height='125px' src={logo} width='125px' />
      </IconButton>
    );
    return this.props.location.pathname === '/login'
      ? logoImg
      : (
        <Link to={isLoggedIn() ? '/account' : '/login'}>
          {logoImg}
        </Link>
      );
  }
}
